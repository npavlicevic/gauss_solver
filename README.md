## Linear system solver based on Gauss elimination
Solve a system of linear equations using augmented form e.g.   
```
x0  + x1  + x2  = 0
2x0 + x1  + x2  = 1
x0  + 2x1 + x2  = 15

```
### Compile and test
- make compile
- make test
- make all

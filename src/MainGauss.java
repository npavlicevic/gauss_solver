import gauss.*;

import java.io.*;
import java.util.*;

public class MainGauss {
  public static void main(String args[]) {
    Gauss gauss = new Gauss();
    int m, n;
    double result_v[] = new double[gauss.MAX_LOCAL_ORDER];
    double local_m[][] = new double[gauss.MAX_LOCAL_ORDER][gauss.MAX_LOCAL_ORDER];
    Scanner in = new Scanner(System.in);
    m = in.nextInt();
    System.out.println("m " + m);
    n = m - 1;
    System.out.println("n " + n);
    gauss.print_vector(result_v, n);
    gauss.read_matrix(local_m, m, n, in);
    gauss.print_matrix(local_m, m, n);
    gauss.gauss(local_m, result_v, n);
    gauss.print_vector(result_v, n);
  }
}

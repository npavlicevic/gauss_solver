package gauss;

import java.io.*;
import java.util.*;

public class Gauss {
  public int MAX_LOCAL_ORDER = 256;
  public double EPS = 0.00001;
  public void read_vector(double local_v[], int n, Scanner in) {
    int i = 0;
    for(; i < n; i++) {
      local_v[i] = in.nextFloat();
    }
  }
  public void print_vector(double local_v[], int n) {
    int i = 0;
    for(; i < n; i++) {
      System.out.print(local_v[i] + " ");
    }
    System.out.println("");
  }
  public void read_matrix(double local_m[][], int m, int n, Scanner in) {
    int i = 0, j = 0;
    for(; i < m; i++) {
      for(j = 0; j < n; j++) {
        local_m[i][j] = in.nextFloat();
      }
    }
  }
  public void print_matrix(double local_m[][], int m, int n) {
    int i = 0,j = 0;
    for(; i < m; i++) {
      for(j = 0; j < n; j++) {
        System.out.print(local_m[i][j] + " ");
      }
      System.out.println("");
    }
  }
  public void gauss(double local_m[][], double local_v[], int n) {
    int i,j,k,maxrow;
    double tmp;

    for(i=0;i<n;i++) {
      // Find the row with the largest first value
      maxrow = i;
      for(j=i+1;j<n;j++) {
        if(Math.abs(local_m[i][j]) > Math.abs(local_m[i][maxrow]))
          maxrow = j;
      }

      // Swap the maxrow and ith row
      for(k=i;k<n+1;k++) {
        tmp = local_m[k][i];
        local_m[k][i] = local_m[k][maxrow];
        local_m[k][maxrow] = tmp;
      }

      // Singular matrix?
      if(Math.abs(local_m[i][i]) < EPS)
        return;

      // Eliminate the ith element of the jth row
      for(j=i+1;j<n;j++) {
        for (k=n;k>=i;k--) {
           local_m[k][j] -= local_m[k][i] * local_m[i][j] / local_m[i][i];
        }
      }
    }

    // Do the back substitution
    for(j=n-1;j>=0;j--) {
      tmp = 0;
      for(k=j+1;k<n;k++)
         tmp += local_m[k][j] * local_v[k];
      local_v[j] = (local_m[n][j] - tmp) / local_m[j][j];
    }
  }
}
